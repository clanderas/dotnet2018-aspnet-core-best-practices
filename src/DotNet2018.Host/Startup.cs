﻿using DotNet2018.Api;
using DotNet2018.Api.Infrastructure.HttpErrors;
using DotNet2018.Application.Ports;
using DotNet2018.Application.Services;
using DotNet2018.Infrastructure;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace DotNet2018.Host
{
    public class Startup
    {        
        public void ConfigureServices(IServiceCollection services)
        {
            Configuration.ConfigureServices(services);

            services
                .AddSingleton<IHttpErrorFactory, DefaultHttpErrorFactory>()
                .AddSingleton<ISpeakerService, SpeakersService>()
                .AddSingleton<INotificationQueue, NotificationQueue>();

            services.AddSwaggerGen(setup =>
            {
                setup.DescribeAllParametersInCamelCase();
                setup.DescribeStringEnumsInCamelCase();
                setup.SwaggerDoc("v1", new Info
                {
                    Title = "Dotnet 2018 Api",
                    Version = "v1"
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            TelemetryConfiguration.Active.DisableTelemetry = true;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            Configuration.Configure(
                app, 
                host => host
                    .UseSwagger()
                    .UseSwaggerUI(setup =>
                    {
                        setup.SwaggerEndpoint("/swagger/v1/swagger.json", "DotNet 2018");
                    })
            );
        }
    }
}